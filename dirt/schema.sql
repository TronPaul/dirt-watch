CREATE TABLE events(id INT PRIMARY KEY, vehicle_class VARCHAR(100));
CREATE TABLE stages(idx INT NOT NULL,
	event_id INT NOT NULL,
	location VARCHAR(100) NOT NULL,
	name VARCHAR(100) NOT NULL,
	time_of_day VARCHAR(100) NOT NULL,
	weather VARCHAR(30) NOT NULL,
	PRIMARY KEY (idx, event_id),
	FOREIGN KEY (event_id) REFERENCES events(id));
CREATE TABLE entries(id INT PRIMARY KEY,
	player_id INT NOT NULL,
        stage_id INT,
        event_id INT NOT NULL,
	name VARCHAR(255) NOT NULL,
	vehicle_name VARCHAR(100) NOT NULL,
	time INT NOT NULL,
	diff INT,
        created DATETIME,
        FOREIGN KEY (event_id) REFERENCES events(id)
        FOREIGN KEY (stage_id) REFERENCES stage(idx));
