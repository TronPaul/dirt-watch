import datetime
import json
from urllib import request
from .dto import Event, Stage, Entry

EVENT_URL_BASE='https://www.dirtgame.com/uk/api/event?assists=any&eventId={event}&group=all&leaderboard=true&nameSearch=&noCache={timestamp}&number=10&page={page}&stageId={stage}&wheel=any'

def parse_time(time_string):
    time, time_frac = time_string.split('.')
    time_parts = time.split(':')
    time_parts.reverse()
    if len(time_parts) > 3:
        raise NotImplementedError()
    millis = int(time_frac)
    for i, tp in enumerate(time_parts):
        millis += int(tp) * 1000 * 60 ** i
    return millis

def create_entry(entry_data):
    if entry_data['DiffFirst'] != '--':
        diff_millis = parse_time(entry_data['DiffFirst'][1:])
    else:
        diff_millis = None
    time_millis = parse_time(entry_data['Time'])
    return Entry(entry_data['PlayerId'], entry_data['Name'], entry_data['VehicleName'],
            time_millis, diff_millis)
        

def scrape_event(event_id):
    ts = int(datetime.datetime.utcnow().timestamp() * 1000)
    event_url = EVENT_URL_BASE.format(event=event_id, timestamp=ts, page=1, stage=0)
    event_data = json.load(request.urlopen(event_url))
    stages = []
    for si in range(1, event_data['TotalStages']):
        su = EVENT_URL_BASE.format(event=event_id, timestamp=ts, page=1, stage=si)
        stage_data = json.load(request.urlopen(su))
        entries = [create_entry(e) for e in stage_data['Entries']]
        stages.append(Stage(stage_data['LocationName'], stage_data['StageName'],
            stage_data['TimeOfDay'], stage_data['WeatherText'], entries))
    entries = [create_entry(e) for e in event_data['Entries']]
    return Event(event_id, event_data['Restriction']['VehicleClass'][0], entries, stages)


def get_active_event(league_id, league_name):
    pass


def scrape_latest_event(league_id, league_name):
    event_id = get_active_event(league_id, league_name)
    return scrape_event(event_id)
