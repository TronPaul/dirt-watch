from .dto import Event, Stage, Entry
import sqlite3
from pathlib import Path


def get_event(conn, event_id):
    cur = conn.cursor()
    cur.execute('SELECT e.vehicle_class as vehicle_class, s.idx as stage_idx, s.location as stage_location, s.name as stage_name, s.time_of_day as stage_time_of_day, s.weather as stage_weather, ee.player_id as entry_player_id, ee.name as entry_name, ee.vehicle_name as entry_vehicle, ee.time as entry_time, ee.diff as entry_diff, ee.created as entry_created FROM events e JOIN entries ee ON ee.event_id = e.id LEFT JOIN STAGES s ON ee.stage_id = s.idx WHERE e.id = ?', (event_id,))
    event = None
    stages = {}
    for r in cur:
        if not event:
            event = Event(event_id, r['vehicle_class'], [], [])
        idx = r['stage_idx']
        if idx:
            if idx not in stages:
                stages[idx] = Stage(r['stage_location'], r['stage_name'], r['stage_time_of_day'], r['stage_weather'], [])
            stages[idx].entries.append(Entry(r['entry_player_id'], r['entry_name'], r['entry_vehicle'], r['entry_time'], r['entry_diff'], r['entry_created']))
        else:
            event.entries.append(Entry(r['entry_player_id'], r['entry_name'], r['entry_vehicle'], r['entry_time'], r['entry_diff'], r['entry_created']))
    for i in sorted(stages.keys()):
        event.stages.append(stages[i])

    return event

def write_updated_and_return_entries(conn, event_old, event_new):
    c = conn.cursor()
    if event_old is None:
        c.execute('INSERT INTO events (id, vehicle_class) VALUES (?, ?)', (event_new.id_, event_new.vehicle_class))
        # assume stages will be added on new event
        for i, s in enumerate(event_new.stages):
            c.execute('INSERT INTO stages (idx, event_id, location, name, time_of_day, weather) VALUES (?, ?, ?, ?, ?)', (i, event_new.id_, s.location, s.name, s.time_of_day, s.weather))
    new_entries = []
    for e in event_new.entries:
        if event_old is None or e not in event_old.entries:
            c.execute('INSERT INTO entries (player_id, event_id, name, vehicle_name, time, diff, created) VALUES (?, ?, ?, ?, ?, ?)', (e.player_id, event_new.id_, e.name, e.vehicle_name, e.time, e.diff, e.created))
            new_entries.append((new_event.id_, None, e))
    for i, s in enumerate(event_new.stages):
        for e in s.entries:
            if e not in event_old.stages[i].entries:
                c.execute('INSERT INTO entries (player_id, stage_id, event_id, name, vehicle_name, time, diff, created) VALUES (?, ?, ?, ?, ?, ?, ?)', (e.player_id, i, event_new.id_, e.name, e.vehicle_name, e.time, e.diff, e.created))
                new_entries.append((new_event.id_, i, e))
    return new_entries

def write_schema(conn):
    with open(str(Path(__file__).parent / 'schema.sql')) as fp:
        conn.executescript(''.join(fp.readlines()))
