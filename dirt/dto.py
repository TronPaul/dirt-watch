import datetime

class Entry:
    def __init__(self, player_id, name, vehicle_name, time, diff, created = None):
        self.player_id = player_id
        self.name = name
        self.vehicle_name = vehicle_name
        self.time = time
        self.diff = diff
        if not created:
            self.created = datetime.datetime.utcnow()

    def __eq__(self, other):
        return (self.player_id == other.player_id and self.name == other.name and
                self.vehicle_name == other.vehicle_name and self.time == other.time and
                self.diff == other.diff)

class Stage:
    def __init__(self, location, name, time_of_day, weather, entries):
        self.location = location
        self.name = name
        self.time_of_day = time_of_day
        self.weather = weather
        self.entries = entries

    def __eq__(self, other):
        return (self.location == other.location and self.name == other.name and
                self.weather == other.weather)

class Event:
    def __init__(self, id_, vehicle_class, entries, stages):
        self.id_ = id_
        self.vehicle_class = vehicle_class
        self.entries = entries
        self.stages = stages

    def __eq__(self, other):
        return self.id_ == other.id_
