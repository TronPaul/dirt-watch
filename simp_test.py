import sqlite3
import dirt.state
c = sqlite3.connect(':memory:')
c.row_factory = sqlite3.Row
dirt.state.write_schema(c)
c.executescript(''.join(open('test_data.sql').readlines()))
c.commit()

e = dirt.state.get_event(c, 0)
print(e)
